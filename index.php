<?php
# namespace \;

define('TIME_START', microtime(true));
define('PATH', '/home/xiaoai/joanna');


// define('CACHE', '\\system\\cache\\APC');

define('ROUTER', '\\system\\router\\web_path');
define('BOX', '\\system\\box');
define('RESPONSE', '\\system\\response');


# @namespace \
# init() import()
require PATH. '/system/def.php';


# @ \system\cache
defined('CACHE') && import(CACHE) && ($cache = init(CACHE));
# @ \system\router
defined('ROUTER') && import(ROUTER) && ($router = init(ROUTER));

import('\\system\\super\\controller');
if( ! import($router->controllerName) ){
	header('Status: 404 Not Found');
	exit("<center>{$router->controllerName} Not found<center>");
}

import('\\system\\super\\model');

$controller = init( $router->controllerName, $router->params);

import('\\system\\super\\box');
defined('BOX') && import(BOX) && ($box = init(BOX));

$controller->run($box);
