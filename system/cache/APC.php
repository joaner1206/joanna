<?php
namespace system\cache;

class APC
{
	public function __construct()
	{
		if( ! extension_loaded('APC') ){
			exit("APC extension not loaded!\n");
		}
	}

	public function get($key)
	{
		return apc_fetch($key);
	}

	public function set($key, $value)
	{
		return apc_add($key, $value);
	}
}
