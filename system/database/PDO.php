<?php
namespace system\database;

class PDO
{
	public $PDO;
	
	public function __construct()
	{
		$dsn = 'mysql:host='.\system\DB::serverName.':'.\system\DB::serverPort;
		try {
			$this->PDO = new \PDO( $dsn, DBFactory::user, DBFactory::pwd);
		}catch (\PDOException $e){
			echo $e->getMessage();
		}
	}
	
}
