<?php
namespace system;

final class box extends \system\super\box
{
	
	public function setTemplate($page, array $data=array(),
					    array $config=array(
							'static' => false,
							'ajax'   => false) )
	{
		extract($data);
		include PATH .'/application/view/'. $page .'.'. \system\super\box::template_extension;
	}
}
