<?php
namespace system;

final class DB
{
	const __default_driver = 'PDO';
	
	const serverName = 'localhost';
	const serverPort = 3306;
	const user = 'root';
	const pwd = 'chilema';

	private static $drivers = array();
	
	public static function getDriver($DriverName = self::__default_driver)
	{
		if( ! array_key_exists($DriverName, self::$drivers) ){
			switch($DriverName)
			{
				case 'PDO':
					import('\\system\\database\\PDO');
					self::$drivers[$DriverName] = init('\\system\\database\\PDO')->PDO;
					break;
				default:
					exit("not support this driver: {$DriverName}");	
			}
		}
		return self::$drivers[$DriverName];
	}
}