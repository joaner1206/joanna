<?php
# namespace \

$lastClass = '';
function import($className)
{
	if( ! class_exists($className) ){
		$classfileName = PATH. str_replace('\\', '/', $className) .'.php';
		if( ! file_exists($classfileName) )
			return false;

		if( isset($GLOBALS['cache']) ){
			$rfc2397 = $GLOBALS['cache']->get($className);
			if( empty($rfc2397) ){
				$rfc2397 = 'data://text/plain;base64,'.base64_encode( file_get_contents($classfileName) );
				$GLOBALS['cache']->set($className, $rfc2397);
			}
			require $rfc2397;
		}else{
			require $classfileName;
		}
	}
	$GLOBALS['lastClass'] = $className;
	return class_exists($className);
}

function init($className, $params = null)
{
	if( ! class_exists($className) ){
		return false;
	}
	return new $className( $params );
}
