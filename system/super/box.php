<?php
namespace system\super;

abstract class box
{
	const template_extension = 'php';

	public $templates;

	abstract function setTemplate($page, 
						array $data=array(), 
						array $config=array(
							'static'=>false, 
							'ajax'=>false
						) );
}
