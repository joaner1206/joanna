<?php
namespace system\super;

interface controller
{
	public function __construct(array $params = null);
	public function run(\system\super\box $box);
}
