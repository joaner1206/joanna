<?php
namespace system\router;

class web_path
{	
	public  $request;
	public  $controllerName = '\\application\\controller\\index';
	public  $params = array();

	public function __construct()
	{
		$this->request = parse_url($_SERVER['REQUEST_URI'])['path'];
		
		$request_arr = explode('/', $this->request);
		if( ! empty($request_arr[1]) ){
			$this->controllerName = '\\application\\controller\\'. $request_arr[1];
			$this->params = array_map("urldecode", array_slice($request_arr, 2));
		}
	}
	
	public function location($from, $to)
	{
		
		$from_arr = explode('/', $from);
		
		foreach( $from_arr as $filedN=>$folder ){
			if( $folder==='*' ){
				if( array_key_exists($this->request_arr, $filedN) )
					$this->params[] = $this->request_arr[$filedN];
			}else	if( strcmp($folder, $this->request_arr[$filedN]) !== 0 ){
				var_dump( $folder, $filedN);
			}
		}
		
		
	}
	
	
}
