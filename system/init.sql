CREATE DATABASE `xiaoai` CHARSET = utf8  COLLATE = utf8_general_ci;
use `xiaoai`;

CREATE TABLE `user`
(
	`id` MEDIUMINT UNSIGNED AUTO_INCREMENT NOT NULL,
	`name` VARCHAR(20) NOT NULL,
	`pwd` BINARY(64) NOT NULL,
	`time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`mail` VARCHAR(100) NOT NULL,
	PRIMARY KEY(`id`),
	INDEX(`name`)
)engine=InnoDB charset=utf8 COLLATE=utf8_general_ci
;

INSERT INTO `user`(`name`, `pwd`, `mail`) 
VALUES ('root', '38e90a00948f9c470c9b5dd00fcc73f0a8b9708a9d3779913dd5dd3f807ada4a', 
'joaner1206@gmail.com');

CREATE TABLE `article`
(
	`id` MEDIUMINT UNSIGNED AUTO_INCREMENT NOT NULL,
	`title` VARCHAR(150) NOT NULL,
	`ascii_title` VARCHAR(150) NOT NULL,
	`body` TEXT,
	`time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`author` MEDIUMINT UNSIGNED NOT NULL,
	PRIMARY KEY(`id`),
	INDEX(`ascii_title`)
)ENGINE=InnoDB charset=utf8 COLLATE=utf8_general_ci
;

INSERT INTO `article` (`title`, `ascii_title`, `body`, `author`)
VALUES ('上线，以后慢慢改吧', 'helloworld', '<p>前前后后也算折腾了半年多了，自己写了一套简易的框架，简易到随时可以花两个小时重写一遍。</p></p>昨天终于辞工了，连续一个多月没休息过，总算能轻松一阵了。然后十月八号又要去新公司报道，discuz!,joomla的二次开发，就靠这几天熟悉一下。虽然平时挺抵触CMS的，甚至是小看，但还真有点担心会做不好。</p><p>:)  等会就把代码上传，马上上线。</p>', 1);