<?php
namespace application\controller;

class index implements \system\super\controller
{
	private $data = array();
	
	public function __construct(array $params = null)
	{
		
	}
	
	public function run(\system\super\box $box)
	{
		$this->data['title'] = 'Welcome!';
		$this->data['article'] = "hi";
		$box->setTemplate('index', $this->data);	
	}
}
